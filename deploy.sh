#!/usr/bin/env sh

# abort on errors
set -e

# build
npm run build

mkdir deploy

move dist deploy -R

cd deploy

git init
git add -A
git commit -m 'deploy'

git push -f git@bitbucket.org:Wilkun/wilkun.bitbucket.io.git master

cd -