import d3 from 'd3'
import functionPlot from 'function-plot'

const VueFunctionPlot = {
	install(Vue) {
		if (VueFunctionPlot.installed) {
			console.warn('It seems like you\'re trying to install vue-function-plot twice.')
			return;
		}
		VueFunctionPlot.installed = true

		// var beaufort = options && options.beaufort ? options.beaufort : require('beaufort-scale');

		Object.defineProperties(Vue.prototype, {
			$functionPlot: {
				get: function () {
					return functionPlot;
				}
			}
		});

		Vue.functionPlot = functionPlot;
	}
}

export default VueFunctionPlot;
