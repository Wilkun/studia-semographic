import Vue from 'vue'
import './plugins/vuetify'
import './plugins/katex'
import './plugins/vue-function-plot'
import App from './App.vue'
import VueFunctionPlot from "./plugins/vue-function-plot";

Vue.config.productionTip = false

Vue.use(VueFunctionPlot)

new Vue({
  render: h => h(App),
}).$mount('#app')
